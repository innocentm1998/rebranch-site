import Image from 'next/image'

import GirlImage from '../../assets/Legal/girl-on-chair.png'
import PageTitle from '../About/PageTitle'


export default function LegalComponent() {
  const pageDetails = {
    title: 'Our Privacy Policy',
    description:
      'Rebranch respects your right to privacy and is committed to safeguarding the privacy of our customers and website visitors. Our privacy policy sets out how we collect and treat your personal information.'
  }

  return (
    <div className='bg-white pt-24'>
      <div className='w-3/4 mx-auto grid grid-cols-1 sm:grid-cols-2 items-center space-y-5'>
        <div className='md:pr-16'>
          <PageTitle title={pageDetails.title} description={pageDetails.description} />
          <a href='/GDPR.pdf' target='_blank' rel='noreferrer'>
            <button className='uppercase mt-8 text-sm text-white bg-blue-900 px-7 py-3 rounded-md'>
              Our Privacy Policy
            </button>
          </a>
        </div>
        <div>
          <Image src={GirlImage} alt='girl-on-chair-image' objectPosition='right' />
        </div>
      </div>
    </div>
  )
}
