import GatsbyImage from '../../assets/Tech/gatsby.png'
import MysqlImage from '../../assets/Tech/mysql.png'
import NodeImage from '../../assets/Tech/node.png'
import PrismaImage from '../../assets/Tech/prisma.png'
import ReactImage from '../../assets/Tech/react.png'
import VuetifyImage from '../../assets/Tech/vuetify.png'
import Redux from '../../assets/Tech/redux.png'
import Next from '../../assets/Tech/next.png'
import Dynamo from '../../assets/Tech/dynamo.png'
import PageTitle from '../About/PageTitle'
import TechCard from './TechCard'

export default function OurTechnology() {
  const technologies = [
    {
      name: 'React',
      description: 'JavaScript framework to build complex user interfaces',
      logo: ReactImage
    },
    {
      name: 'Node',
      description: 'JavaScript runtime which allows us to write a server in JavaScript',
      logo: NodeImage
    },
    {
      name: 'Gatsby',
      description: 'React based JavaScript framework for writing performant accessible web applications',
      logo: GatsbyImage
    },
    {
      name: 'Prisma',
      description: 'A modern JavaScript ORM',
      logo: PrismaImage
    },
    {
      name: 'MySQL',
      description: 'A popular type of relational database',
      logo: MysqlImage
    },
    {
      name: 'React-Redux',
      description: 'React Redux is the official React UI bindings layer for Redux',
      logo: Redux
    },
    {
      name: 'NextJS',
      description:
        'A framework for developing single page Javascript applications that maximises speed and performance',
      logo: Next
    },
    {
      name: 'DynamoDB',
      description: 'A fully managed NoSQL database service',
      logo: Dynamo
    }
  ]

  const pageDetails = {
    title: 'Our Technology',
    description: 'These are the tools we prefer, to bring you modern apps quickly.'
  }

  return (
    <div className='bg-white text-center pt-16'>
      <PageTitle title={pageDetails.title} description={pageDetails.description} center />
      <div className='mt-14 w-3/4 xl:w-3/5 mx-auto grid grid-cols-1 lg:grid-cols-2 gap-8 2xl:gap-20'>
        {technologies.map(({ name, description, logo }, i) => (
          <TechCard key={i} title={name} description={description} image={logo} />
        ))}
      </div>
    </div>
  )
}
