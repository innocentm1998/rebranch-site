import OurTechnology from '../components/Tech/OurTechnology'
import Banner from '../components/utils/Banner'
import BaseLayout from '../layouts/Base'
import { pageRoutes } from '../lib/routes'

export default function Tech() {
  return (
    <BaseLayout
      title='Tech | Rebranch'
      selectedNavKey='tech'
      description='Rebranch is an Australian software development consultancy and product company. Our work spans across development, design, and marketing services.'
      canonical={pageRoutes.tech}
    >
      <Banner pageTitle='Tech' breadcrumbLabel='Home - Tech' />
      <OurTechnology />
    </BaseLayout>
  )
}
