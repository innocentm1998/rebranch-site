import { ArrowCircleLeftIcon } from '@heroicons/react/solid'
import { FC } from 'react'

interface Props {
  dir: 'left' | 'right'
  handleClick: (i: number) => void
  disabled?: boolean
}

const ArrowButton: FC<Props> = ({ dir, handleClick, disabled }) => {
  const i = dir === 'left' ? 0 : 1
  return (
    <ArrowCircleLeftIcon
      width={35}
      height={35}
      className={`cursor-pointer ${i === 1 && 'transform rotate-180'} ${
        disabled ? 'text-gray-400 pointer-events-none' : 'text-prim1'
      }`}
      onClick={() => handleClick(i)}
    />
  )
}

export default ArrowButton
