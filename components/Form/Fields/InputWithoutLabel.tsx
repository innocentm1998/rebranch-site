import { FC } from 'react'

interface Props {
  type: 'text' | 'email' | 'password'
  label?: string
  placeholder?: string
}

const InputWithoutLabel: FC<Props> = ({ label, placeholder, type }) => {
  return (
    <div>
      <label htmlFor={`${type}`} className='sr-only'>
        {label || type}
      </label>
      <input
        type={`${type}`}
        name={`${label || type}`}
        id={`${label || type}`}
        className='py-3 px-5 shadow-sm focus:ring-gray-600 focus:border-gray-600 block w-full sm:text-sm border border-gray-300 rounded-md'
        placeholder={`${placeholder || ''}`}
      />
    </div>
  )
}

export default InputWithoutLabel
