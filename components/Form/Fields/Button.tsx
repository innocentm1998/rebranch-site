import { FC } from 'react'

interface Props {
  name: string
}

const Button: FC<Props> = ({ name }) => {
  return <div className='py-1 bg-prim2 text-white flex justify-center align-center w-full rounded'>{name}</div>
}

export default Button
