interface Props {
  title: string
  description?: string
  center?: boolean
  titleColor?: string
  size?: string
}

const PageTitle = ({ title, description, center, titleColor, size }: Props) => {
  let _color = titleColor ? `text-${titleColor}` : 'text-black'
  let _size = size ? `text-${size}` : 'text-4xl'
  return (
    <div className={`${center && 'text-center'}`}>
      <h1 className={`font-bold font-heading ${_size} ${_color}`}>{title}</h1>
      {description && <p className={`mt-3 text-gray-500 leading-5 ${center && 'w-4/5 mx-auto'}`}>{description}</p>}
    </div>
  )
}

export default PageTitle
