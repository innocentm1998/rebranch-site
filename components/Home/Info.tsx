import InfoCard from './InfoCard'

const SupportSvg = () => (
  <svg width='72' height='72' viewBox='0 0 72 72' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M58.5 43.875C58.5 42.0848 57.7888 40.3679 56.523 39.102C55.2571 37.8362 53.5402 37.125 51.75 37.125H20.25C18.4598 37.125 16.7429 37.8362 15.477 39.102C14.2112 40.3679 13.5 42.0848 13.5 43.875V46.125C13.5 54.9945 21.87 64.125 36 64.125C50.13 64.125 58.5 54.9945 58.5 46.125V43.875ZM18 43.875C18 43.2783 18.2371 42.706 18.659 42.284C19.081 41.8621 19.6533 41.625 20.25 41.625H51.75C52.3467 41.625 52.919 41.8621 53.341 42.284C53.7629 42.706 54 43.2783 54 43.875V46.125C54 52.596 47.556 59.625 36 59.625C24.444 59.625 18 52.596 18 46.125V43.875Z'
      fill='white'
    />
    <path
      d='M48.375 20.2508C48.3752 17.5829 47.5132 14.9865 45.9175 12.8484C44.3219 10.7104 42.078 9.14523 39.5204 8.3863C36.9628 7.62736 34.2284 7.71528 31.7248 8.63694C29.2212 9.5586 27.0825 11.2647 25.6275 13.5008H18C17.4033 13.5008 16.831 13.7378 16.409 14.1598C15.9871 14.5818 15.75 15.1541 15.75 15.7508V24.7508C15.75 24.8228 15.75 24.8903 15.759 24.9578H15.75V29.2508C15.75 31.041 16.4612 32.7579 17.727 34.0238C18.9929 35.2896 20.7098 36.0008 22.5 36.0008H24.2415C24.7053 36.0005 25.1615 35.8833 25.5679 35.66C25.9744 35.4367 26.318 35.1145 26.567 34.7233C26.8159 34.332 26.9623 33.8843 26.9924 33.4215C27.0225 32.9588 26.9355 32.4959 26.7394 32.0756C26.5432 31.6554 26.2443 31.2914 25.8703 31.0173C25.4962 30.7431 25.059 30.5678 24.5992 30.5073C24.1394 30.4469 23.6718 30.5033 23.2396 30.6715C22.8074 30.8396 22.4246 31.114 22.1265 31.4693C21.6019 31.381 21.1256 31.1097 20.7821 30.7036C20.4386 30.2974 20.2501 29.7827 20.25 29.2508V27.0008H21.375C22.7115 27.0008 23.958 26.6093 25.0065 25.9388C26.2802 28.4032 28.3449 30.3681 30.8693 31.5182C33.3937 32.6683 36.2313 32.9369 38.9267 32.2809C41.622 31.6248 44.0188 30.0822 45.7322 27.9005C47.4456 25.7189 48.3764 23.0248 48.375 20.2508ZM28.125 20.2508C28.125 18.1622 28.9547 16.1592 30.4315 14.6823C31.9084 13.2055 33.9114 12.3758 36 12.3758C38.0886 12.3758 40.0916 13.2055 41.5685 14.6823C43.0453 16.1592 43.875 18.1622 43.875 20.2508C43.875 22.3394 43.0453 24.3424 41.5685 25.8193C40.0916 27.2961 38.0886 28.1258 36 28.1258C33.9114 28.1258 31.9084 27.2961 30.4315 25.8193C28.9547 24.3424 28.125 22.3394 28.125 20.2508ZM23.625 20.2508C23.625 20.8475 23.3879 21.4198 22.966 21.8418C22.544 22.2637 21.9717 22.5008 21.375 22.5008H20.25V18.0008H23.625V20.2508Z'
      fill='white'
    />
  </svg>
)

const DeliverySvg = () => (
  <svg width='72' height='72' viewBox='0 0 72 72' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M66.3345 19.4036L43.8345 7.02861C43.5023 6.84584 43.1292 6.75 42.75 6.75C42.3708 6.75 41.9978 6.84584 41.6655 7.02861L19.1655 19.4036C18.8128 19.5978 18.5186 19.8832 18.3137 20.2299C18.1088 20.5766 18.0007 20.9719 18.0007 21.3746C18.0007 21.7773 18.1088 22.1726 18.3137 22.5193C18.5186 22.866 18.8128 23.1514 19.1655 23.3456L40.5 35.0794V59.1971L33.669 55.4396L31.5 59.3794L41.6655 64.9706C41.9976 65.1538 42.3708 65.2499 42.75 65.2499C43.1293 65.2499 43.5024 65.1538 43.8345 64.9706L66.3345 52.5956C66.6874 52.4015 66.9818 52.1162 67.1868 51.7695C67.3918 51.4228 67.5 51.0274 67.5 50.6246V21.3746C67.5 20.9718 67.3918 20.5765 67.1868 20.2298C66.9818 19.8831 66.6874 19.5978 66.3345 19.4036ZM42.75 11.5691L60.5813 21.3746L42.75 31.1801L24.9188 21.3746L42.75 11.5691ZM63 49.2949L45 59.1949V35.0771L63 25.1771V49.2949Z'
      fill='white'
    />
    <path d='M22.5 36H4.5V31.5H22.5V36Z' fill='white' />
    <path d='M27 54H9V49.5H27V54Z' fill='white' />
    <path d='M31.5 45H13.5V40.5H31.5V45Z' fill='white' />
  </svg>
)

const LaptopSvg = () => (
  <svg width='72' height='72' viewBox='0 0 72 72' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M61.5 12H10.5C8.84315 12 7.5 13.3431 7.5 15V45C7.5 46.6569 8.84315 48 10.5 48H61.5C63.1568 48 64.5 46.6569 64.5 45V15C64.5 13.3431 63.1568 12 61.5 12Z'
      stroke='white'
      strokeWidth='5'
    />
    <path d='M6 60H66' stroke='white' strokeWidth='5' strokeLinecap='round' strokeLinejoin='round' />
    <path d='M33 21H39' stroke='white' strokeWidth='5' strokeLinecap='round' strokeLinejoin='round' />
  </svg>
)

const Info = () => {
  const sections = [
    {
      label: 'Initial Consultation',
      desc: 'We have an initial meeting to understand the scope of the project. We always request clients provide us with as much information and/ or resources as possible so we can develop the right solution with them.',
      Icon: SupportSvg,
      gradientStops: 'from-purple-900 to-indigo-900'
    },
    {
      label: 'Delivery Process',
      desc: "This is a streamlined process to transition from the initial meeting to the final hand off. We work in an agile manner - meaning we provide weekly or fortnightly updates to clients and create a feedback loop to ensure that what we deliver meets our client's needs.",
      Icon: DeliverySvg,
      gradientStops: 'from-blue-900 to-indigo-900'
    },
    {
      label: 'Technologies Used',
      desc: "We have experience with the software at the forefront of development, in addition to traditional and legacy tools. We use a NextJs and Amazon Web Services based technology 'stack' for software development to maximise efficiency, and minimise future client costs in regards to security issues and updates.",
      Icon: LaptopSvg,
      gradientStops: 'from-indigo-800 to-indigo-900'
    }
  ]

  return (
    <div className='w-full bg-white bg-infoBg bg-no-repeat bg-right-bottom bg-contain'>
      <div className='w-4/5 mx-auto md:grid grid-cols-3 gap-x-8 xl:gap-x-20 text-white text-center space-y-5 md:space-y-0'>
        {sections.map(({ label, desc, Icon, gradientStops }, i) => (
          <InfoCard key={i} Icon={Icon} label={label} desc={desc} bg={gradientStops} idx={i} />
        ))}
      </div>
    </div>
  )
}

export default Info
