import TitledDescription from './TitledDescription'
import TitledDescription2 from './TitledDescription2'

import RoadmapComponent from './RoadmapComponent'

import DocImage from '../../assets/Software/document.png'
import ContactImage from '../../assets/Software/contact.png'
import MeetingImage from '../../assets/Software/conversation.png'
import ClockImage from '../../assets/Software/clock.png'

const Development = () => {
  const data = {
    main: {
      title: 'Development',
      description:
        'We tailor our software solution to your business needs. Our expertise covers custom applications - web and mobile, cloud migrations, database migrations, security audits and more. '
    },
    sub1: {
      title: 'Consultation Process',
      description: `Our consultation process is fairly straight forward. We provide our clients with a frank and fair assessment of their technological requirements. No business is the same so we believe in creating a custom solution for each and every client.`
    }
  }

  const RoadmapData = [
    {
      image: DocImage,
      title: 'Prepare',
      description:
        'You will be conveying your idea to us so make sure to bring anything that will help you with that such as example sites, apps, or wireframes.'
    },
    {
      image: ContactImage,
      title: 'Contact Us',
      description: 'Send us an email or call us so we can arrange a time and location from there.'
    },
    {
      image: MeetingImage,
      title: 'Meeting',
      description:
        'We work through your idea, or business pain points to ensure that we are both on the same page, and align what we will propose with your vision.'
    },
    {
      image: ClockImage,
      title: 'Time Estimates',
      description: 'After the meeting we send an email containing a time and cost estimate breakdown of your project requirements.'
    }
  ]

  return (
    <div className='bg-white'>
      <div className='w-4/5 mx-auto pt-24 space-y-12'>
        <TitledDescription title={data.main.title} description={data.main.description} />
        <TitledDescription2 title={data.sub1.title} description={data.sub1.description} />
      </div>
      <RoadmapComponent data={RoadmapData} />
    </div>
  )
}

export default Development
