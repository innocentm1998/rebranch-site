import { useForm, ValidationError } from '@formspree/react'
import { LocationMarkerIcon, PhoneIcon, MailIcon } from '@heroicons/react/solid'
import { Fragment, useEffect, useState } from 'react'
import { Transition } from '@headlessui/react'
import { CheckCircleIcon } from '@heroicons/react/outline'
import { XIcon } from '@heroicons/react/solid'
import React from 'react'

const Fb = () => (
  <svg width='40' height='40' viewBox='0 0 48 48' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M24.0001 0.959839C11.2753 0.959839 0.960083 11.275 0.960083 23.9998C0.960083 36.7246 11.2753 47.0398 24.0001 47.0398C36.7249 47.0398 47.0401 36.7246 47.0401 23.9998C47.0401 11.275 36.7249 0.959839 24.0001 0.959839ZM29.4577 16.8814H25.9945C25.5841 16.8814 25.1281 17.4214 25.1281 18.139V20.6398H29.4601L28.8049 24.2062H25.1281V34.9126H21.0409V24.2062H17.3329V20.6398H21.0409V18.5422C21.0409 15.5326 23.1289 13.087 25.9945 13.087H29.4577V16.8814Z'
      fill='#BAC4F6'
    />
  </svg>
)

const Ig = () => (
  <svg width='40' height='40' viewBox='0 0 48 48' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M31.2001 23.9998C31.2001 25.9094 30.4415 27.7407 29.0913 29.091C27.741 30.4413 25.9096 31.1998 24.0001 31.1998C22.0905 31.1998 20.2592 30.4413 18.9089 29.091C17.5587 27.7407 16.8001 25.9094 16.8001 23.9998C16.8001 23.5894 16.8433 23.1886 16.9177 22.7998H14.4001V32.3926C14.4001 33.0598 14.9401 33.5998 15.6073 33.5998H32.3953C32.715 33.5992 33.0215 33.4717 33.2474 33.2454C33.4732 33.0191 33.6001 32.7124 33.6001 32.3926V22.7998H31.0825C31.1569 23.1886 31.2001 23.5894 31.2001 23.9998ZM24.0001 28.7998C24.6306 28.7997 25.2549 28.6753 25.8373 28.4339C26.4198 28.1925 26.949 27.8387 27.3947 27.3928C27.8404 26.9468 28.1939 26.4174 28.4351 25.8349C28.6762 25.2523 28.8002 24.6279 28.8001 23.9974C28.7999 23.3669 28.6756 22.7426 28.4342 22.1602C28.1927 21.5777 27.8389 21.0486 27.393 20.6028C26.9471 20.1571 26.4177 19.8036 25.8351 19.5625C25.2525 19.3213 24.6282 19.1973 23.9977 19.1974C22.7243 19.1978 21.5033 19.7039 20.6031 20.6045C19.7029 21.5052 19.1974 22.7265 19.1977 23.9998C19.198 25.2732 19.7041 26.4943 20.6048 27.3945C21.5054 28.2946 22.7267 28.8002 24.0001 28.7998ZM29.7601 18.9598H32.6377C32.8289 18.9598 33.0122 18.8841 33.1477 18.7491C33.2831 18.6141 33.3594 18.431 33.3601 18.2398V15.3622C33.3601 15.1706 33.284 14.9869 33.1485 14.8514C33.013 14.7159 32.8293 14.6398 32.6377 14.6398H29.7601C29.5685 14.6398 29.3847 14.7159 29.2493 14.8514C29.1138 14.9869 29.0377 15.1706 29.0377 15.3622V18.2398C29.0401 18.6358 29.3641 18.9598 29.7601 18.9598ZM24.0001 0.959839C17.8895 0.959839 12.0292 3.38726 7.70834 7.7081C3.3875 12.0289 0.960083 17.8893 0.960083 23.9998C0.960083 30.1104 3.3875 35.9707 7.70834 40.2916C12.0292 44.6124 17.8895 47.0398 24.0001 47.0398C27.0257 47.0398 30.0218 46.4439 32.8171 45.286C35.6125 44.1282 38.1524 42.431 40.2918 40.2916C42.4313 38.1521 44.1284 35.6122 45.2863 32.8169C46.4441 30.0215 47.0401 27.0255 47.0401 23.9998C47.0401 20.9742 46.4441 17.9782 45.2863 15.1828C44.1284 12.3875 42.4313 9.84756 40.2918 7.7081C38.1524 5.56864 35.6125 3.87152 32.8171 2.71365C30.0218 1.55579 27.0257 0.959839 24.0001 0.959839ZM36.0001 33.3334C36.0001 34.7998 34.8001 35.9998 33.3337 35.9998H14.6665C13.2001 35.9998 12.0001 34.7998 12.0001 33.3334V14.6662C12.0001 13.1998 13.2001 11.9998 14.6665 11.9998H33.3337C34.8001 11.9998 36.0001 13.1998 36.0001 14.6662V33.3334Z'
      fill='#BAC4F6'
    />
  </svg>
)

const Li = () => (
  <svg width='40' height='40' viewBox='0 0 48 48' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M24 0.959961C11.2752 0.959961 0.959961 11.2752 0.959961 24C0.959961 36.7248 11.2752 47.04 24 47.04C36.7248 47.04 47.04 36.7248 47.04 24C47.04 11.2752 36.7248 0.959961 24 0.959961ZM18.36 33.5496H13.6944V18.5352H18.36V33.5496ZM15.9984 16.692C14.5248 16.692 13.572 15.648 13.572 14.3568C13.572 13.0392 14.5536 12.0264 16.0584 12.0264C17.5632 12.0264 18.4848 13.0392 18.5136 14.3568C18.5136 15.648 17.5632 16.692 15.9984 16.692ZM35.4 33.5496H30.7344V25.2288C30.7344 23.292 30.0576 21.9768 28.3704 21.9768C27.0816 21.9768 26.316 22.8672 25.9776 23.724C25.8528 24.0288 25.8216 24.4608 25.8216 24.8904V33.5472H21.1536V23.3232C21.1536 21.4488 21.0936 19.8816 21.0312 18.5328H25.0848L25.2984 20.6184H25.392C26.0064 19.6392 27.5112 18.1944 30.0288 18.1944C33.0984 18.1944 35.4 20.2512 35.4 24.672V33.5496Z'
      fill='#BAC4F6'
    />
  </svg>
)

const Tw = () => (
  <svg width='40' height='40' viewBox='0 0 48 48' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M24.0001 0.959839C11.2753 0.959839 0.960083 11.275 0.960083 23.9998C0.960083 36.7246 11.2753 47.0398 24.0001 47.0398C36.7249 47.0398 47.0401 36.7246 47.0401 23.9998C47.0401 11.275 36.7249 0.959839 24.0001 0.959839ZM33.3721 19.8334C33.3817 20.0302 33.3841 20.227 33.3841 20.419C33.3841 26.419 28.8217 33.3334 20.4745 33.3334C18.008 33.3375 15.5928 32.6292 13.5193 31.2934C13.8721 31.3366 14.2345 31.3534 14.6017 31.3534C16.7281 31.3534 18.6841 30.631 20.2369 29.4118C19.2906 29.3933 18.3737 29.0799 17.614 28.5155C16.8542 27.9511 16.2895 27.1637 15.9985 26.263C16.6781 26.3923 17.3781 26.3652 18.0457 26.1838C17.0186 25.9761 16.095 25.4196 15.4314 24.6086C14.7678 23.7976 14.4051 22.7821 14.4049 21.7342V21.679C15.0169 22.0174 15.7177 22.2238 16.4617 22.2478C15.4988 21.6069 14.8172 20.6222 14.5562 19.4954C14.2952 18.3685 14.4746 17.1844 15.0577 16.1854C16.1976 17.587 17.6191 18.7336 19.2302 19.551C20.8414 20.3683 22.6062 20.8382 24.4105 20.9302C24.1811 19.9566 24.2798 18.9344 24.6913 18.0227C25.1028 17.1109 25.804 16.3606 26.6859 15.8885C27.5677 15.4163 28.5809 15.2488 29.5678 15.4119C30.5548 15.5749 31.4602 16.0595 32.1433 16.7902C33.1589 16.5893 34.1328 16.2168 35.0233 15.6886C34.6848 16.7402 33.9761 17.6331 33.0289 18.2014C33.9285 18.0931 34.807 17.8512 35.6353 17.4838C35.0269 18.3956 34.2604 19.1913 33.3721 19.8334Z'
      fill='#BAC4F6'
    />
  </svg>
)

const Form = () => {
  const [state, handleSubmit] = useForm('mrgjglyp')
  const [submitted, onSubmitted] = useState(false)

  return (
    <>
      <div
        aria-live='assertive'
        className='fixed inset-0 flex items-end px-4 py-6 pointer-events-none sm:p-6 sm:items-start'
      >
        <div className='w-full flex flex-col items-center space-y-4 sm:items-end'>
          {/* Notification panel, dynamically insert this into the live region when it needs to be displayed */}
          <Transition
            show={state.succeeded && submitted}
            as={Fragment}
            enter='transform ease-out duration-300 transition'
            enterFrom='translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2'
            enterTo='translate-y-0 opacity-100 sm:translate-x-0'
            leave='transition ease-in duration-100'
            leaveFrom='opacity-100'
            leaveTo='opacity-0'
          >
            <div className='max-w-sm w-full bg-white shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden'>
              <div className='p-4'>
                <div className='flex items-start'>
                  <div className='flex-shrink-0'>
                    <CheckCircleIcon className='h-6 w-6 text-green-400' aria-hidden='true' />
                  </div>
                  <div className='ml-3 w-0 flex-1 pt-0.5'>
                    <p className='text-sm font-medium text-gray-900'>Email sent!</p>
                  </div>
                  <div className='ml-4 flex-shrink-0 flex'>
                    <button
                      className='bg-white rounded-md inline-flex text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                      onClick={() => {
                        onSubmitted(false)
                      }}
                    >
                      <span className='sr-only'>Close</span>
                      <XIcon className='h-5 w-5' aria-hidden='true' />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </Transition>
        </div>
      </div>

      <div className='relative bg-white h-auto md:h-96 py-0 md:py-80'>
        <div className='md:absolute w-full py-12 top-0 transform md:-translate-y-1/4 lg:-translate-y-1/3'>
          <div className='w-4/5 mx-auto bg-white rounded-sm shadow-xl md:grid grid-cols-9 p-8 pb-12 gap-8 space-y-12 md:space-y-0'>
            <div className='col-span-5 lg:col-span-6'>
              <p className='font-medium text-3xl'>Send Us a Message</p>
              <form className='mt-8 space-y-12' onSubmit={handleSubmit}>
                <div>
                  <input
                    id='email'
                    type='email'
                    name='email'
                    className='w-full p-3 border-2 border-gray-400 rounded-sm'
                    placeholder='Your Email Address'
                  />
                  <ValidationError prefix='Email' errors={state.errors} field='email' />
                </div>
                <div>
                  <textarea
                    id='message'
                    name='message'
                    rows={5}
                    className='w-full p-3 border-2 border-gray-400 rounded-sm'
                    placeholder='Message here...'
                  />
                  <ValidationError prefix='Message' errors={state.errors} field='Message' />
                </div>
                <button
                  type='submit'
                  disabled={state.submitting}
                  className='uppercase bg-prim2 text-white py-2 w-full rounded-sm'
                  onClick={() => {
                    state?.submitting ? onSubmitted(false) : onSubmitted(true)
                  }}
                >
                  Submit
                </button>
              </form>
            </div>
            <div className='col-span-4 lg:col-span-3 space-y-12'>
              <p className='font-medium text-3xl'>Contact Information</p>
              <div className='space-y-6 md:space-y-16'>
                <div className='flex items-start space-x-4'>
                  <MailIcon height={35} width={35} fill='#156CB2' className='flex-shrink-0 flex-grow-0' />
                  <p className='text-gray-600 w-full break-all'>info@rebranch.com.au</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Form
