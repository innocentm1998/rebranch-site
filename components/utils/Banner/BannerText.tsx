import { FC } from "react";

interface Props {
  title: string
  desc?: string
  breadcrumb: string
}

const BannerText:FC<Props> = ({title, desc, breadcrumb}) => {
  return (
    <>
      <h1 className='font-heading font-bold text-5xl sm:text-7xl'>{title}</h1>
      {desc && <p className='md:w-3/4 mt-4 text-xl sm:text-2xl'>{desc}</p>}
      <p className='uppercase font-heading mt-8 text-lg'>{breadcrumb}</p>
    </>
  )
}

export default BannerText;