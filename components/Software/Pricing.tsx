import TitledDescription2 from './TitledDescription2'

const Pricing = () => {
  const data = {
    title: 'Services',
    description:
      'We provide a high quality service and strive to cater for businesses of all sizes. Please get in touch with us to discuss pricing'
  }

  const serviceList = [
    {
      name: 'Business Starter Kit',
      description:
        'From corporate branding - logo, email signatures etc. - to your website and marketing. We will be with you every step of the way to fulfilling your business goals with dedicated support. ',
      appBuilderPrice: '$1 000',
      customPrice: '$2 500',
      maintainancePrice: (
        <>
          from <b>$50</b> per hour excluding App Builder fees
        </>
      )
    },
    {
      name: 'Web Application',
      description:
        'For clients with special requirements, whether you need to interact with and store customer data, or develop an internal or client facing interface, we can build it.',
      appBuilderPrice: '',
      customPrice: '$5 000',
      maintainancePrice: (
        <>
          from <b>$75</b> per hour
        </>
      )
    },
    {
      name: 'Mobile Application',
      description:
        'Cross-platform software dominates mobile markets. For your dedicated internal or client facing mobile application, partner with us for a seamless experience.',
      appBuilderPrice: '',
      customPrice: '$10 000',
      maintainancePrice: (
        <>
          from <b>$75</b> per hour
        </>
      )
    },
    {
      name: 'Custom Software Solution',
      description:
        'Whether a web application with a dedicated mobile experience,  or a IP related client or customer facing application, unique software requirements need unique software solutions. We are here to work with you to deliver exactly what you need. ',
      appBuilderPrice: '',
      customPrice: '$10 000',
      maintainancePrice: (
        <>
          from <b>$100</b> per hour
        </>
      )
    },
    {
      name: 'Minimum Viable Product',
      description:
        'Innovation takes courage. We ourselves are innovators and would love to help others on their journey’s. If you have an idea, that you truly want to invest in, and build out your vision, without taking too high financial risk, we can work with you to figure out what makes your idea unique, and then build it with you.',
      appBuilderPrice: '',
      customPrice: '$10 000',
      maintainancePrice: (
        <>
          from <b>$150</b> per hour
        </>
      )
    }
  ]

  const pricingHeaders = [
    {
      title: 'Services',
      bg: 'bg-prim1'
    },
    {
      title: 'Service Description',
      bg: 'bg-blue-900'
    }
  ]

  return (
    <div className='bg-white'>
      <div className='w-4/5 mx-auto pt-24 space-y-12'>
        <TitledDescription2 title={data.title} description={data.description} />
        <div className='mt-12 overflow-x-scroll lg:overflow-x-hidden overflow-y-hidden'>
          <div className='min-w-lg lg:min-w-0'>
            <div className='grid grid-cols-2 gap-5 lg:gap-10 text-white'>
              {pricingHeaders.map(({ title, bg }) => (
                <div
                  key={title}
                  className={`font-medium lg:text-xl ${bg} p-6 flex items-center justify-center text-center`}
                >
                  {title}
                </div>
              ))}
            </div>

            <div className='mt-12 space-y-16'>
              {serviceList.map(({ name, description }) => (
                <div key={name} className='grid grid-cols-2 gap-5 lg:gap-10 items-start text-gray-800'>
                  <div className='px-6 py-10 lg:text-xl text-center font-medium bg-gray-100 text-gray-900'>{name}</div>

                  <div className='leading-tight text-sm lg:text-base'>{description}</div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Pricing
