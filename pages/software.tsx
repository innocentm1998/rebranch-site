import Development from '../components/Software/Development'
import Pricing from '../components/Software/Pricing'
import Partners from '../components/Home/Partners'
import Product from '../components/Software/Product'
import Banner from '../components/utils/Banner'
import BaseLayout from '../layouts/Base'
import { pageRoutes } from '../lib/routes'

export default function Software() {
  return (
    <BaseLayout
      title='Software | Rebranch'
      selectedNavKey='software'
      description='Rebranch is an Australian software development consultancy and product company. Our work spans across development, design, and marketing services.'
      canonical={pageRoutes.software}
    >
      <Banner
        pageTitle='Software'
        description='Empowering businesses, through technology.'
        breadcrumbLabel='Home - Software'
      />
      <Development />
      <Pricing />
      {/* <Partners /> */}
    </BaseLayout>
  )
}
