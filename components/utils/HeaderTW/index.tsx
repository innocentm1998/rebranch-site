/* This example requires Tailwind CSS v2.0+ */
import { Fragment, ReactNode } from 'react'
import { Popover, Transition } from '@headlessui/react'

import { XIcon } from '@heroicons/react/outline'

import Image from 'next/image'

import WhiteLogo from "../../../assets/Header/White_Logo.png"

import { ChevronDownIcon } from '@heroicons/react/solid'
import { pageRoutes } from '../../../lib/routes'
import { headerText } from '../../../lib/constants/headers'
import { navKeys } from '../../../lib/types'
import CustomButton from '../../ui/CustomButton'

function classNames(...classes: any) {
  return classes.filter(Boolean).join(' ')
}

const MenuIcon = (props: { className?: string }) => (
  <svg
    width='48'
    height='48'
    viewBox='0 0 48 48'
    className={props.className ?? ''}
    fill='currentcolor'
    xmlns='http://www.w3.org/2000/svg'
  >
    <path d='M8 22H32V26H8V22ZM8 12H40V16H8V12ZM8 36H22.47V32H8V36Z' fill='white' />
  </svg>
)

interface NavKeyProp {
  name: string
  href: string
  navKey: navKeys
}
const navigationLinks: NavKeyProp[] = [
  {
    name: headerText.home,
    href: pageRoutes.home,
    navKey: 'home'
  },
  {
    name: headerText.software,
    href: pageRoutes.software,
    navKey: 'software'
  },
  {
    name: headerText.tech,
    href: pageRoutes.tech,
    navKey: 'tech'
  },
]
export default function HeaderTW(props: { selectedNavKey: navKeys }) {
  const DesktopHref = (p: { name: string; href: string; navKey: navKeys }) => {
    return (
      <a
        href={p.href}
        className={`font-heading inline-flex border-b-2 border-transparent items-center text-center uppercase text-white hover:text-gray-100 ${
          props.selectedNavKey === p.navKey && 'border-white'
        }`}
      >
        {p.name}
      </a>
    )
  }

  const DesktopListPop = (p: { name: string; list: any[]; children: ReactNode; navKey: navKeys }) => {
    return (
      <Popover className='relative'>
        {({ open }) => (
          <>
            <Popover.Button
              className={classNames(
                open ? 'text-gray-100' : props.selectedNavKey === p.navKey ? 'text-prim1' : 'text-white',
                'group rounded-md inline-flex items-center text-base font-medium hover:text-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-prim1'
              )}
            >
              <span>{p.name}</span>
              <ChevronDownIcon
                className={classNames(open ? 'text-gray-100' : 'text-white', 'ml-2 h-5 w-5 group-hover:text-gray-100')}
                aria-hidden='true'
              />
            </Popover.Button>

            <Transition
              show={open}
              as={Fragment}
              enter='transition ease-out duration-200'
              enterFrom='opacity-0 translate-y-1'
              enterTo='opacity-100 translate-y-0'
              leave='transition ease-in duration-150'
              leaveFrom='opacity-100 translate-y-0'
              leaveTo='opacity-0 translate-y-1'
            >
              <Popover.Panel
                static
                className='absolute left-1/2 z-20 transform -translate-x-1/2 mt-3 px-2 w-screen max-w-md sm:px-0'
              >
                <div className='rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden'>
                  <div className='relative grid gap-6 bg-white px-5 py-6 sm:gap-8 sm:p-8'>
                    {p.list.map(item => (
                      <a
                        key={item.name}
                        href={item.href}
                        className='-m-3 p-3 flex items-start rounded-lg hover:bg-gray-50'
                      >
                        <item.icon className='flex-shrink-0 h-6 w-6 text-prim1' aria-hidden='true' />
                        <div className='ml-4'>
                          <p className='text-base font-medium text-gray-900'>{item.name}</p>
                          <p className='mt-1 text-sm text-gray-500'>{item.description}</p>
                        </div>
                      </a>
                    ))}
                  </div>
                  {p.children}
                </div>
              </Popover.Panel>
            </Transition>
          </>
        )}
      </Popover>
    )
  }

  return (
    <Popover className='relative shadow-2xl bg-transparent'>
      {({ open }) => (
        <div className='w-full md:w-11/12 left-1/2 transform -translate-x-1/2 absolute z-10'>
          <div className='flex justify-between items-center lg:items-start'>
            <a href={pageRoutes.home} className='mt-4 sm:flex-shrink-0'>
              <div>
                <Image src={WhiteLogo} height={80} objectFit='contain' objectPosition='left center' alt='Rebranch' />
              </div>
            </a>
            <div className='mr-4 lg:hidden'>
              <Popover.Button className='rounded-md p-2 inline-flex items-center justify-center bg-white text-white bg-opacity-0 hover:bg-opacity-20 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-prim1'>
                <span className='sr-only'>Open menu</span>
                {open ? <XIcon className='h-10 w-10' /> : <MenuIcon className='h-10 w-10' aria-hidden='true' />}
              </Popover.Button>
            </div>

            <Popover.Group as='nav' className='hidden py-6 lg:flex justify-center lg:justify-end flex-wrap gap-4'>
              {navigationLinks.map(e => {
                return <DesktopHref key={e.navKey} {...e} />
              })}
              <CustomButton name='Contact Us' href={pageRoutes.contact} outlined />
            </Popover.Group>
          </div>

          <Transition
            show={open}
            as={Fragment}
            enter='duration-200 ease-out'
            enterFrom='opacity-0 transform scale-95'
            enterTo='opacity-100 transform scale-100'
            leave='duration-100 ease-in'
            leaveFrom='opacity-100 transform scale-100'
            leaveTo='opacity-0 transform scale-95'
          >
            <Popover.Panel className='absolute right-0 z-20 p-2 transition transform origin-top-right lg:hidden w-full sm:w-1/2'>
              <div className='rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-gradient-to-b from-prim1 to-prim2 text-white'>
                <div className='grid gap-y-4 gap-x-8 p-6 py-10 sm:py-8 text-center justify-center'>
                  {navigationLinks.map(e => {
                    return (
                      <a key={`${e.navKey}-m`} href={e.href} className={`font-heading uppercase text-lg divide-y divide-black hover:text-gray-300`}>
                        <span className={`p-1 ${e.navKey === props.selectedNavKey && 'border-b-2'}`}>{e.name}</span>
                      </a>
                    )
                  })}
                  <CustomButton name='Contact Us' href={pageRoutes.contact} />
                </div>
              </div>
            </Popover.Panel>
          </Transition>
        </div>
      )}
    </Popover>
  )
}
