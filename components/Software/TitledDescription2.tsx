interface Props {
  title: string,
  description: string
}

const TitledDescription2 = ({title, description}: Props) => {
  return (
    <div className='w-4/5 mx-auto text-center'>
      <p className='text-2xl text-prim1 font-medium'>{title}</p>
      <p className="text-gray-600 text-sm md:text-base leading-5">{description}</p>
    </div>
  )
}

export default TitledDescription2;