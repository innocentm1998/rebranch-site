export const pageRoutes = {
  home: '/',
  software: '/software',
  tech: '/tech',
  contact: '/contact',
  legal: '/legal'
}

export const pageRoutesPriorities = {
  home: '1.0',
  software: '1.0',
  tech: '0.8',
  contact: '0.8',
  legal: '0.8'
}
