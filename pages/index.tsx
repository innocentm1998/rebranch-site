import Head from 'next/head'
import HomeIntro from '../components/Home/Intro'
import Partners from '../components/Home/Partners'
import BaseLayout from '../layouts/Base'
import { pageRoutes } from '../lib/routes'
import HomeFAQs from '../components/Home/HomeFAQs'
import WhatWeDo from '../components/Home/WhatWeDo'
import Info from '../components/Home/Info'
import Subscription from '../components/Home/Subscription'

export default function Home() {
  return (
    <BaseLayout
      title='Rebranch'
      selectedNavKey='home'
      description='Rebranch is an Australian software development consultancy and product company.'
      canonical={pageRoutes.home}
    >
      <HomeIntro />
      <WhatWeDo />
      <Info />
      {/* <Partners /> */}
    </BaseLayout>
  )
}
