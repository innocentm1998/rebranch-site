import Image from "next/image";

import GroupImage from "../../assets/About/Group.png"
import BulletPoints from "./BulletPoints";

import PageTitle from "./PageTitle";

const AboutComponent = () => {

  const points = [
    "Quality Assurance Standardization",
    "Fast and Clean Design",
    "Meet The Right Needs of Customers",
    "Images and Text Are Clearly Eye-catching"
  ]

  const pageDetails = {
    title: 'Who We Are',
    description:
      'We offer a wide range of technology-based solutions that include:'
  }

  return (
    <div className='w-full bg-white pt-28'>
      <div className='w-4/5 mx-auto sm:grid grid-cols-2 items-center gap-4 space-y-8 sm:space-y-0'>
        <div className="space-y-8">
          <PageTitle title={pageDetails.title} description={pageDetails.description} />
          <BulletPoints pointsArray={points} />
        </div>
        <div>
          <Image src={GroupImage} alt="group-image" height={720} objectFit="contain" />
        </div>
      </div>
    </div>
  )
}

export default AboutComponent;