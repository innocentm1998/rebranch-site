import Image from 'next/image'

import WDImage from '../../assets/Home/wwd-1.png'
import TCImage from '../../assets/Home/wwd-2.png'
import TAImage from '../../assets/Home/wwd-3.png'
import CSImage from '../../assets/Home/wwd-4.png'
import ADImage from '../../assets/Home/wwd-5.png'
import { pageRoutes } from '../../lib/routes'
import PageTitle from '../About/PageTitle'
import AltImageDescription from './AltImageDescription'

export default function WhatWeDo() {
  const whatWeDo = [
    {
      label: 'Application Development',
      desc: 'We build boutique websites and applications, based on client needs.',
      image: WDImage,
      link: pageRoutes.software
    },
    {
      label: 'Technology Consulting',
      desc: 'We advise businesses on how to best utilise technology to drive their operations. Whether it is on a new project, internal software based resources, or redeveloping existing technology infrastructure for future needs. We help businesses go online in the most cost efficient and optimal way. We also help our clients to analyse their existing cloud infrastructure and improve their data storage and processing efficiencies.',
      image: TCImage,
      link: pageRoutes.software
    },
  ]

  return (
    <div className='w-full bg-white'>
      <div className='w-4/5 mx-auto py-24 text-center'>
        <PageTitle
          title='What We Do'
          description='We offer a wide range of technology-based solutions that include:'
          titleColor='prim1'
          size='5xl'
        />
        <AltImageDescription arrayData={whatWeDo} />
      </div>
    </div>
  )
}
