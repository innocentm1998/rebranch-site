import { CheckCircleIcon } from "@heroicons/react/solid"

interface Props {
  pointsArray: string[]
}

const BulletPoints = ({pointsArray}: Props) => {
  return (
    <div className='space-y-2'>
      {pointsArray.map((point, i) => (
        <div key={i} className='flex space-x-4 items-center text-gray-500'>
          <CheckCircleIcon height={24} width={24} fill='#156CB2' className='flex-shrink-0' />
          <p>{point}</p>
        </div>
      ))}
    </div>
  )
}

export default BulletPoints
