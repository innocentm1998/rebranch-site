import Image from 'next/image'

import MapImage from '../../assets/Contact/map.png'
import BannerText from '../utils/Banner/BannerText'

const ContactBanner = () => {
  return (
    <div className='relative w-full bg-prim2 h-96 sm:h-auto'>
      <div className=''>
        <Image src={MapImage} alt='map' objectFit='cover' />
      </div>
      <div className='absolute h-full w-full bg-gradient-to-r from-prim1 to-prim2mild top-0 flex items-center'>
        <div className='w-3/4 mx-auto text-white mt-10 py-28'>
          <BannerText title='Contact Us' breadcrumb='Home - Contact Us' />
        </div>
      </div>
    </div>
  )
}

export default ContactBanner
