import BannerText from "./BannerText"

interface BannerProps {
  pageTitle: string
  description?: string
  breadcrumbLabel: string
}

const Banner = ({ pageTitle, description, breadcrumbLabel }: BannerProps) => {
  return (
    <div className='w-full bg-gradient-to-r from-prim1 to-prim2 pb-24 pt-40'>
      <div className='w-3/4 mx-auto text-white'>
        <BannerText title={pageTitle} desc={description??undefined} breadcrumb={breadcrumbLabel} />
      </div>
    </div>
  )
}

export default Banner
