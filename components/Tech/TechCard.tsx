import Image from "next/image"

interface Props {
  title: string
  description: string
  image: any
}

const TechCard = ({ title, description, image }: Props) => {
  return (
    <div className='p-8 flex justify-between bg-purple-100 rounded'>
      <div className='text-left'>
        <p className='text-xl font-medium'>{title}</p>
        <p className='leading-5 text-gray-600 text-sm'>{description}</p>
      </div>
      <div className='relative w-4/5 md:w-3/4 lg:w-1/2 flex justify-end'>
        <Image src={image} alt={title} objectFit='contain' layout='intrinsic' />
      </div>
    </div>
  )
}

export default TechCard
