import { CursorClickIcon } from '@heroicons/react/outline'
import { headerText } from '../../../lib/constants/headers'
import { pageRoutes } from '../../../lib/routes'

import Image from 'next/image'

import Logo from '../../../assets/Footer/Color_Logo.png'

const FacebookIcon = (props: any) => (
  <svg width='45' height='45' viewBox='0 0 48 48' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M24.0001 0.959961C11.2753 0.959961 0.960083 11.2752 0.960083 24C0.960083 36.7248 11.2753 47.04 24.0001 47.04C36.7249 47.04 47.0401 36.7248 47.0401 24C47.0401 11.2752 36.7249 0.959961 24.0001 0.959961ZM29.4577 16.8816H25.9945C25.5841 16.8816 25.1281 17.4216 25.1281 18.1392V20.64H29.4601L28.8049 24.2064H25.1281V34.9128H21.0409V24.2064H17.3329V20.64H21.0409V18.5424C21.0409 15.5328 23.1289 13.0872 25.9945 13.0872H29.4577V16.8816Z'
      fill='#BAC4F6'
    />
  </svg>
)

const InstagramIcon = (props: any) => (
  <svg width='45' height='45' viewBox='0 0 48 48' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M31.2001 24C31.2001 25.9095 30.4415 27.7409 29.0913 29.0911C27.741 30.4414 25.9096 31.2 24.0001 31.2C22.0905 31.2 20.2592 30.4414 18.9089 29.0911C17.5587 27.7409 16.8001 25.9095 16.8001 24C16.8001 23.5896 16.8433 23.1888 16.9177 22.8H14.4001V32.3928C14.4001 33.06 14.9401 33.6 15.6073 33.6H32.3953C32.715 33.5993 33.0215 33.4719 33.2474 33.2455C33.4732 33.0192 33.6001 32.7125 33.6001 32.3928V22.8H31.0825C31.1569 23.1888 31.2001 23.5896 31.2001 24ZM24.0001 28.8C24.6306 28.7998 25.2549 28.6755 25.8373 28.434C26.4198 28.1926 26.949 27.8388 27.3947 27.3929C27.8404 26.9469 28.1939 26.4176 28.4351 25.835C28.6762 25.2524 28.8002 24.6281 28.8001 23.9976C28.7999 23.3671 28.6756 22.7428 28.4342 22.1603C28.1927 21.5779 27.8389 21.0487 27.393 20.603C26.9471 20.1572 26.4177 19.8037 25.8351 19.5626C25.2525 19.3214 24.6282 19.1974 23.9977 19.1976C22.7243 19.1979 21.5033 19.704 20.6031 20.6046C19.7029 21.5053 19.1974 22.7266 19.1977 24C19.198 25.2733 19.7041 26.4944 20.6048 27.3946C21.5054 28.2947 22.7267 28.8003 24.0001 28.8ZM29.7601 18.96H32.6377C32.8289 18.96 33.0122 18.8842 33.1477 18.7492C33.2831 18.6143 33.3594 18.4311 33.3601 18.24V15.3624C33.3601 15.1708 33.284 14.987 33.1485 14.8515C33.013 14.7161 32.8293 14.64 32.6377 14.64H29.7601C29.5685 14.64 29.3847 14.7161 29.2493 14.8515C29.1138 14.987 29.0377 15.1708 29.0377 15.3624V18.24C29.0401 18.636 29.3641 18.96 29.7601 18.96ZM24.0001 0.959961C17.8895 0.959961 12.0292 3.38738 7.70834 7.70822C3.3875 12.0291 0.960083 17.8894 0.960083 24C0.960083 30.1105 3.3875 35.9709 7.70834 40.2917C12.0292 44.6125 17.8895 47.04 24.0001 47.04C27.0257 47.04 30.0218 46.444 32.8171 45.2861C35.6125 44.1283 38.1524 42.4312 40.2918 40.2917C42.4313 38.1522 44.1284 35.6123 45.2863 32.817C46.4441 30.0216 47.0401 27.0256 47.0401 24C47.0401 20.9743 46.4441 17.9783 45.2863 15.1829C44.1284 12.3876 42.4313 9.84768 40.2918 7.70822C38.1524 5.56876 35.6125 3.87164 32.8171 2.71378C30.0218 1.55591 27.0257 0.959961 24.0001 0.959961ZM36.0001 33.3336C36.0001 34.8 34.8001 36 33.3337 36H14.6665C13.2001 36 12.0001 34.8 12.0001 33.3336V14.6664C12.0001 13.2 13.2001 12 14.6665 12H33.3337C34.8001 12 36.0001 13.2 36.0001 14.6664V33.3336Z'
      fill='#BAC4F6'
    />
  </svg>
)

const TwitterIcon = (props: any) => (
  <svg width='45' height='45' viewBox='0 0 48 48' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M24.0001 0.959961C11.2753 0.959961 0.960083 11.2752 0.960083 24C0.960083 36.7248 11.2753 47.04 24.0001 47.04C36.7249 47.04 47.0401 36.7248 47.0401 24C47.0401 11.2752 36.7249 0.959961 24.0001 0.959961ZM33.3721 19.8336C33.3817 20.0304 33.3841 20.2272 33.3841 20.4192C33.3841 26.4192 28.8217 33.3336 20.4745 33.3336C18.008 33.3377 15.5928 32.6293 13.5193 31.2936C13.8721 31.3368 14.2345 31.3536 14.6017 31.3536C16.7281 31.3536 18.6841 30.6312 20.2369 29.412C19.2906 29.3934 18.3737 29.08 17.614 28.5156C16.8542 27.9512 16.2895 27.1638 15.9985 26.2632C16.6781 26.3924 17.3781 26.3653 18.0457 26.184C17.0186 25.9763 16.095 25.4197 15.4314 24.6088C14.7678 23.7978 14.4051 22.7822 14.4049 21.7344V21.6792C15.0169 22.0176 15.7177 22.224 16.4617 22.248C15.4988 21.607 14.8172 20.6224 14.5562 19.4955C14.2952 18.3686 14.4746 17.1846 15.0577 16.1856C16.1976 17.5872 17.6191 18.7338 19.2302 19.5511C20.8414 20.3685 22.6062 20.8384 24.4105 20.9304C24.1811 19.9567 24.2798 18.9345 24.6913 18.0228C25.1028 17.111 25.804 16.3607 26.6859 15.8886C27.5677 15.4165 28.5809 15.2489 29.5678 15.412C30.5548 15.5751 31.4602 16.0596 32.1433 16.7904C33.1589 16.5894 34.1328 16.2169 35.0233 15.6888C34.6848 16.7403 33.9761 17.6332 33.0289 18.2016C33.9285 18.0932 34.807 17.8513 35.6353 17.484C35.0269 18.3957 34.2604 19.1914 33.3721 19.8336Z'
      fill='#BAC4F6'
    />
  </svg>
)

const LinkedinIcon = (props: any) => (
  <svg width='45' height='45' viewBox='0 0 48 48' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M24 0.959961C11.2752 0.959961 0.959961 11.2752 0.959961 24C0.959961 36.7248 11.2752 47.04 24 47.04C36.7248 47.04 47.04 36.7248 47.04 24C47.04 11.2752 36.7248 0.959961 24 0.959961ZM18.36 33.5496H13.6944V18.5352H18.36V33.5496ZM15.9984 16.692C14.5248 16.692 13.572 15.648 13.572 14.3568C13.572 13.0392 14.5536 12.0264 16.0584 12.0264C17.5632 12.0264 18.4848 13.0392 18.5136 14.3568C18.5136 15.648 17.5632 16.692 15.9984 16.692ZM35.4 33.5496H30.7344V25.2288C30.7344 23.292 30.0576 21.9768 28.3704 21.9768C27.0816 21.9768 26.316 22.8672 25.9776 23.724C25.8528 24.0288 25.8216 24.4608 25.8216 24.8904V33.5472H21.1536V23.3232C21.1536 21.4488 21.0936 19.8816 21.0312 18.5328H25.0848L25.2984 20.6184H25.392C26.0064 19.6392 27.5112 18.1944 30.0288 18.1944C33.0984 18.1944 35.4 20.2512 35.4 24.672V33.5496Z'
      fill='#BAC4F6'
    />
  </svg>
)

const links = {
  helpful: [
    {
      label: 'Contact Us',
      link: pageRoutes.contact
    },
    {
      label: 'Legal',
      link: pageRoutes.legal
    }
  ],
  quick: [
    {
      label: 'Software',
      link: pageRoutes.software
    }
  ],
  social: [
    {
      Icon: FacebookIcon,
      link: 'https://facebook.com/RebranchAU'
    },
    {
      Icon: InstagramIcon,
      link: 'https://instagram.com/RebranchAu'
    },
    {
      Icon: LinkedinIcon,
      link: 'https://www.linkedin.com/company/rebranchau/'
    },
    {
      Icon: TwitterIcon,
      link: 'https://twitter.com/RebranchAU'
    }
  ]
}

export default function Footer() {
  return (
    <footer className='bg-white relative' aria-labelledby='footer-heading'>
      <div className='bg-footerBackground bg-cover pt-2 sm:pt-56'>
        <div className='w-11/12 mx-auto flex flex-wrap justify-between pb-20 border-indigo-300 border-b-2'>
          <div className='w-full sm:w-1/2 lg:w-2/6 mt-8'>
            <Image src={Logo} alt='Rebranch Logo' height={85} objectFit='contain' objectPosition='left' />
            <p className='text-sm text-gray-500 leading-5'>
              Rebranch is an Australian software development consultancy and product company.
            </p>
            <div className='mt-8 flex items-start space-x-3'>
              {links.social.map(({ Icon, link }, i) => (
                <a key={i} target='_blank' rel='noreferrer' href={link}>
                  <Icon />
                </a>
              ))}
            </div>
          </div>
          <div className='w-full lg:w-3/5 mt-4 flex flex-wrap justify-between'>
            <div className='w-2/5 sm:w-3/12 mt-4'>
              <p className='font-bold font-heading text-2xl'>Helpful Links</p>
              <ul className='mt-4'>
                {links.helpful.map(({ label, link }) => (
                  <a className='text-gray-600 hover:text-blue-800' key={link} href={link}>
                    <p className='mt-1 leading-tight'>{label}</p>
                  </a>
                ))}
              </ul>
            </div>
            <div className='w-2/5 sm:w-3/12 mt-4'>
              <p className='font-bold font-heading text-2xl'>Quick Links</p>
              <ul className='mt-4'>
                {links.quick.map(({ label, link }) => (
                  <a className='text-gray-600 hover:text-blue-800' key={link} href={link}>
                    <p className='mt-1 leading-tight'>{label}</p>
                  </a>
                ))}
              </ul>
            </div>
            <div className='w-full sm:w-5/12 mt-4'>
              <p className='font-bold font-heading text-2xl'>Contact</p>
              <a href='mailto:info@rebranch.com.au' className='leading-tight text-gray-600'>
                info@rebranch.com.au
              </a>
            </div>
          </div>
        </div>
        <div className='text-center py-6'>
          <p className=''>©2024 Rebranch Technologies Enteprises Pty Ltd, All Rights Reserved</p>
        </div>
      </div>
    </footer>
  )
}
