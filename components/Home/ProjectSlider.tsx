import { Transition } from '@headlessui/react'
import Image from 'next/image'
import { FC, useState } from 'react'
import PageTitle from '../About/PageTitle'
import CustomButton from '../ui/CustomButton'
import ArrowButton from './ArrowButton'

interface Project {
  image: any
  title: string
  desc: string
  learnLink: string
  webLink: string
}

interface SliderProps {
  ProjectArray: Project[]
}

const ProjectSlider: FC<SliderProps> = ({ ProjectArray }) => {
  const [activeIndex, setActiveIndex] = useState(0)
  const [transform, setTransform] = useState('translate-x-1/2')

  const handleArrowClick = (i: number) => {
    if (i === 1) {
      setTransform('translate-x-1/2')
      setActiveIndex(val => val + 1)
    } else {
      setTransform('-translate-x-1/2')
      setActiveIndex(val => val - 1)
    }
  }

  return (
    <div>
      <div className='w-4/5 mx-auto flex justify-center overflow-hidden'>
        {ProjectArray.map(({ image }, i) => (
          <Transition
            key={i}
            show={i === activeIndex}
            enter={`transition-all duration-200`}
            enterFrom={`transform  ${transform}`}
            enterTo='transform translate-x-0'
            leave={`transition-none`}
            leaveTo={``}
          >
            <div key={i} className='py-10'>
              <Image src={image} alt='project-image' width={650} objectFit='contain' />
            </div>
          </Transition>
        ))}
      </div>
      <div className='w-4/5 mx-auto grid grid-cols-10 justify-between items-center'>
        <div className='col-span-5 sm:col-span-2 flex justify-start text-gray-400 order-1 sm:order-none'>
          <ArrowButton dir='left' handleClick={handleArrowClick} disabled={activeIndex === 0} />
        </div>
        <div className='col-span-10 sm:col-span-6 mb-8 overflow-hidden'>
          {ProjectArray.map(({ title, desc, learnLink, webLink }, i) => (
            <Transition
              key={i}
              show={i === activeIndex}
              enter={`transition-all duration-500`}
              enterFrom={`transform  ${transform}`}
              enterTo='transform translate-x-0'
              leave={`transition-none`}
              leaveTo={``}
            >
              <div>
                <PageTitle title={title} description={desc} />
                <div className='mt-6 flex gap-y-3 flex-col md:flex-row justify-center items-center md:space-x-5'>
                  <CustomButton name='Learn More' href={learnLink} accent='prim2' />
                  <CustomButton name='Visit Website' href={webLink} accent='prim2' outlined />
                </div>
              </div>
            </Transition>
          ))}
        </div>
        <div className='col-span-5 sm:col-span-2 flex justify-end text-prim1 order-2 sm:order-none'>
          <ArrowButton dir='right' handleClick={handleArrowClick} disabled={activeIndex === ProjectArray.length - 1} />
        </div>
      </div>
    </div>
  )
}

export default ProjectSlider
