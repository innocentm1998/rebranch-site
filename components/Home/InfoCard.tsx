import { FC } from 'react'

interface Props {
  Icon: () => JSX.Element
  label: string
  desc: string
  bg: string
  idx: number
}

const InfoCard: FC<Props> = ({ Icon, bg, desc, label, idx }) => {
  return (
    <div className={`${idx === 1 ? 'md:pt-32' : idx === 2 && 'md:pt-64'}`}>
      <div className={`rounded-xl bg-gradient-to-r ${bg} p-10 md:p-5 lg:p-14`}>
        <div className='mx-auto w-min my-4'>
          <Icon />
        </div>
        <p className='text-xl lg:text-2xl font-medium font-heading'>{label}</p>
        <p className='leading-6 text-sm'>{desc}</p>
      </div>
    </div>
  )
}

export default InfoCard
