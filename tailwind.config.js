module.exports = {
  purge: [
    './components/**/*.tsx',
    './components/**/**/*.tsx',
    './components/**/**/**/*.tsx',
    './pages/**/*.tsx',
    './pages/*.tsx',
    './layouts/**/*.tsx'
  ],

  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: ['Poppins', 'Lato', 'sans-serif'],
      heading: ['Nunito', 'sans-serif', 'system-ui']
    },
    extend: {
      colors: {
        transparent: 'transparent',
        prim1: '#662d91',
        prim2: '#2e2c71',
        sec1: '#156CB2',
        sec2: '#23366D',
        prim2mild: "#2e2c7180"
      },
      backgroundImage: {
        footerBackground: "url('./assets/Footer/Background.svg')",
        homeIntro: "url(./assets/Home/Background.svg)",
        infoBg: "url(./assets/Home/info-bg.svg)"
      },
      minWidth: {
        lg: "1024px"
      }
    }
  },
  variants: {
    extend: {
      scale: ['group-hover']
    }
  },
  plugins: []
}
