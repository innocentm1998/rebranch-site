interface Props {
  title: string,
  description: string | string[]
}

const TitledDescription = ({title, description}: Props) => {
  return (
    <div className='sm:grid grid-cols-5 space-y-5 sm:space-y-0'>
      <div className='col-span-2 md:col-span-1'>
        <p className='font-heading text-3xl md:text-2xl lg:text-3xl font-medium'>{title}</p>
      </div>
      <div className='col-span-3 md:col-span-4 text-gray-600 text-sm md:text-base leading-5 space-y-4'>
        {
          Array.isArray(description) ? (
            description.map((desc, i) => (
              <p key={i}>{desc}</p>
            ))
          ) : (
            <p>{description}</p>
          )
        }
      </div>
    </div>
  )
}

export default TitledDescription;