import React from 'react'
import Image from 'next/image'

interface DataObject {
  image: any
  title: string
  description: string
}

interface Props {
  data: DataObject[]
}

const RoadmapComponent = ({ data }: Props) => {
  return (
    <div className='w-11/12 sm:w-4/5 mx-auto mt-24'>
      {data.map(({ image, title, description }, i) => (
        <React.Fragment key={i}>
          <div className={`flex justify-start sm:justify-end ${i % 2 !== 0 && 'sm:flex-row-reverse'}`}>
            <div className='w-1/5 flex justify-center items-center flex-shrink-0'>
              <Image src={image} alt={title} />
            </div>

            <div className={`w-auto sm:w-2/5 ${i % 2 !== 0 && 'sm:text-right'} p-4`}>
              <h3 className='pt-0 font-bold uppercase'>{title}</h3>
              <p className='leading-5 text-gray-500 text-sm sm:text-base'>{description}</p>
            </div>
          </div>
          {i < data.length - 1 && (
            <div className='w-1/5 sm:mx-auto h-36 sm:h-52 flex justify-center'>
              <div className='w-1 border-dashed border-r-2 border-prim1'></div>
            </div>
          )}
        </React.Fragment>
      ))}
    </div>
  )
}

export default RoadmapComponent
