export const headerText = {
  home: 'Home',
  tech: 'Tech',
  contact: 'Contact',
  software: 'Software',
  legal: 'Legal'
}
