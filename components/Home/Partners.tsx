import PerdamanImg from '../../assets/Partners/Perdaman.png'
import LifeCorpusImg from '../../assets/Partners/LifeCorpus.png'
import MedelaImg from '../../assets/Partners/Medela.png'
import Image from 'next/image'
const partners = [
  {
    link: 'https://perdaman.com.au',
    img: PerdamanImg,
    alt: 'Perdaman'
  },
  {
    link: 'https://www.medelahealth.com.au/',
    img: MedelaImg,
    alt: 'Medela Health'
  },
  {
    link: 'https://www.lifecorpus.com.au/',
    img: LifeCorpusImg,
    alt: 'LifeCorpus'
  }
]

export default function Partners() {
  return (
    <div className='bg-white'>
      <div className='max-w-7xl mx-auto py-8 px-4 sm:px-6 lg:py-8 lg:px-8 mt-8'>
        <p className='text-center text-2xl sm:text-4xl xxl:text-6xl font-semibold uppercase text-prim1 tracking-wider'>
          Our Partners
        </p>
        <div className='grid grid-cols-1 gap-0.5 md:grid-cols-3 lg:mt-8 mt-2'>
          {partners.map(partner => (
            <div className='col-span-1 flex justify-center py-8 px-8' key={partner.link}>
              <a href={partner.link} target='_blank' rel='noreferrer'>
                <Image height={120} width={160} src={partner.img} alt={partner.alt} />
              </a>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}
