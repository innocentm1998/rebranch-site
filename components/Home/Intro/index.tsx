import CustomButton from '../../ui/CustomButton'

import Image from 'next/image'

import { pageRoutes } from '../../../lib/routes'

import GroupImage from '../../../assets/Home/intro.png'

export default function HomeIntro() {
  return (
    <div className='w-full bg-prim2 bg-cover bg-center py-28'>
      <div className='w-4/5 mx-auto text-white flex justify-between flex-wrap items-center space-y-12'>
        <div className='w-full text-center sm:text-left md:w-1/2'>
          <h1 className='font-heading text-5xl sm:text-6xl xl:text-8xl font-mono'>Rebranch</h1>
          <p className='text-xl md:text-2xl'>Technical excellence, delivered consistently...</p>
          <div className='flex flex-col items-center sm:items-start lg:flex-row gap-2 lg:gap-4 mt-10'>
            <CustomButton name='Learn More' href={pageRoutes.software} />
          </div>
        </div>
        <div className='w-full md:w-1/2'>
          <Image src={GroupImage} alt='group-image' />
        </div>
      </div>
    </div>
  )
}
