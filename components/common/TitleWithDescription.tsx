import { FC } from 'react'

interface Props {
  title: string
  description: string
}

const TitleWithDescription: FC<Props> = ({ title, description }) => {
  return (
    <div>
      <h1>{title}</h1>
      <p className='text-gray-500'>{description}</p>
    </div>
  )
}

export default TitleWithDescription
