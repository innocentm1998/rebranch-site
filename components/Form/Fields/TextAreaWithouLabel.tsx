import { FC } from 'react'

interface Props {
  label?: string
  name: string
  placeholder?: string
}

const TextAreaWithoutLabel: FC<Props> = ({ label, name, placeholder }) => {
  return (
    <div>
      <label htmlFor={`${name}`} className='sr-only'>
        {label || name}
      </label>
      <textarea
        className='py-3 px-5 shadow-sm focus:ring-gray-600 focus:border-gray-600 block w-full sm:text-sm border border-gray-300 rounded-md resize-y'
        id={`${label || name}`}
        name={`${label || name}`}
        placeholder={`${placeholder || ''}`}
        rows={8}
      />
    </div>
  )
}

export default TextAreaWithoutLabel
