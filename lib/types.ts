export type navKeys = 'home' | 'contact' | 'tech' | 'software' | 'legal'

export interface TimelineDataObject {
  slug?: string
  createdAt: string
  icon: string
  title: string
  excerpt: string
  type: 'GENERIC' | string
}

export interface PostObject {
  slug: string
  title: string
  createdAt: Date
  lastUpdate?: Date
  html: string
  images?: string[]
}
