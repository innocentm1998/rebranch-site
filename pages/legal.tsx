import LegalComponent from '../components/Legal'
import Banner from '../components/utils/Banner'
import BaseLayout from '../layouts/Base'
import { pageRoutes } from '../lib/routes'

export default function Legal() {
  return (
    <BaseLayout
      title='Legal | Rebranch'
      selectedNavKey='legal'
      description='Rebranch is an Australian software development consultancy and product company. Our work spans across development, design, and marketing services.'
      canonical={pageRoutes.legal}
    >
      <Banner pageTitle='Legal' breadcrumbLabel='Home - Legal' />
      <LegalComponent />
    </BaseLayout>
  )
}
