interface Props {
  name: string
  href: string
  outlined?: boolean
  accent?: string
  fullWidth?: boolean
}

export default function CustomButton({name, href, outlined, accent, fullWidth}: Props) {
  const color = accent ?? "white";
  const text = accent ? "white" : "prim1";
  const border = `border-${color}`

  const layoutClasses = outlined ? `text-${color} bg-transparent` : `text-${text} bg-${color}`;

  const buttonWidth = fullWidth ? "w-full" : "w-max";

  return (
    <button className={`${buttonWidth} rounded-md shadow`}>
      <a
        href={href}
        className={`font-heading flex items-center justify-center px-6 py-2 border-2 font-medium rounded-md ${layoutClasses} ${border} uppercase`}
      >
        {name}
      </a>
    </button>
  )
}
