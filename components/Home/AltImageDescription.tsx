import { FC } from 'react'
import Image from "next/image"
import CustomButton from '../ui/CustomButton'
import PageTitle from '../About/PageTitle'

interface PropObject {
  image: any
  label: string
  desc: string
  link: string
}

interface Props {
  arrayData: PropObject[]
}

const AltImageDescription:FC<Props> = ({ arrayData }) => {
  return (
    <div className='text-center sm:text-left mt-14 space-y-14 lg:space-y-0'>
      {arrayData.map(({ image, label, desc, link }, i) => (
        <div key={i} className={`grid sm:grid-cols-2 gap-x-8 items-center justify-between`}>
          <div className={`relative ${i % 2 !== 0 && 'sm:order-2'}`}>
            <Image width={520} objectFit='contain' src={image} alt={label} objectPosition='left' />
          </div>
          <div className='space-y-4'>
            <PageTitle title={label} description={desc} />
            <CustomButton href={link} name='Read More' outlined accent='prim2' />
          </div>
        </div>
      ))}
    </div>
  )
}

export default AltImageDescription
