import ContactBanner from '../components/Contact/ContactBanner'
import Form from '../components/Contact/Form'
import LegalComponent from '../components/Legal'
import Banner from '../components/utils/Banner'
import BaseLayout from '../layouts/Base'
import { pageRoutes } from '../lib/routes'

export default function Contact() {
  return (
    <BaseLayout
      title='Contact | Rebranch'
      selectedNavKey='contact'
      description='Rebranch is an Australian software development consultancy and product company. Our work spans across development, design, and marketing services.'
      canonical={pageRoutes.contact}
    >
      <ContactBanner />
      <Form />
    </BaseLayout>
  )
}
