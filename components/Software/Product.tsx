import TitledDescription from './TitledDescription'
import TitledDescription2 from './TitledDescription2'

import Image from 'next/image'

import Page1Image from '../../assets/Software/page1.png'
import Page2Image from '../../assets/Software/page2.png'
import Page3Image from '../../assets/Software/page3.png'

const KniveSvg = () => (
  <svg width='32' height='32' viewBox='0 0 32 32' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path d='M11 2H9V12H11V2Z' fill='#662D91' />
    <path
      d='M14 11C14 12.0609 13.5786 13.0783 12.8284 13.8284C12.0783 14.5786 11.0609 15 10 15C8.93913 15 7.92172 14.5786 7.17157 13.8284C6.42143 13.0783 6 12.0609 6 11V2H4V11C4.00144 12.4169 4.50425 13.7875 5.41939 14.8692C6.33452 15.9509 7.60294 16.6739 9 16.91V30H11V16.91C12.3971 16.6739 13.6655 15.9509 14.5806 14.8692C15.4958 13.7875 15.9986 12.4169 16 11V2H14V11Z'
      fill='#662D91'
    />
    <path
      d='M22 1.99996H21V30H23V20H26C26.5304 20 27.0391 19.7892 27.4142 19.4142C27.7893 19.0391 28 18.5304 28 18V7.99996C28.0309 7.20374 27.8969 6.40972 27.6063 5.66777C27.3157 4.92582 26.8749 4.25197 26.3114 3.68853C25.748 3.12509 25.0741 2.68424 24.3322 2.39365C23.5902 2.10307 22.7962 1.96903 22 1.99996ZM26 18H23V4.08996C25.88 4.64996 26 7.62996 26 7.99996V18Z'
      fill='#662D91'
    />
  </svg>
)

const IndustrySvg = () => (
  <svg width='32' height='32' viewBox='0 0 32 32' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M29.53 6.15C29.378 6.06223 29.2055 6.01603 29.03 6.01603C28.8545 6.01603 28.682 6.06223 28.53 6.15L20 10.38V7C19.9995 6.82954 19.9554 6.66203 19.8719 6.5134C19.7885 6.36477 19.6684 6.23994 19.5231 6.15076C19.3778 6.06159 19.2122 6.01104 19.0419 6.0039C18.8715 5.99677 18.7022 6.03329 18.55 6.11L10 10.38V3C10 2.73478 9.89464 2.48043 9.70711 2.29289C9.51957 2.10536 9.26522 2 9 2H3C2.73478 2 2.48043 2.10536 2.29289 2.29289C2.10536 2.48043 2 2.73478 2 3V28H30V7C30.0003 6.83006 29.9574 6.66283 29.8751 6.51411C29.7929 6.36538 29.6741 6.24007 29.53 6.15ZM22 26H18V19H22V26ZM28 26H24V18C24 17.7348 23.8946 17.4804 23.7071 17.2929C23.5196 17.1054 23.2652 17 23 17H17C16.7348 17 16.4804 17.1054 16.2929 17.2929C16.1054 17.4804 16 17.7348 16 18V26H4V4H8V13.62L18 8.62V13.62L28 8.62V26Z'
      fill='#662D91'
    />
  </svg>
)

const KnightSvg = () => (
  <svg width='32' height='32' viewBox='0 0 32 32' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M22.9999 23H22.8684L23.8319 21.5547C23.9414 21.3904 23.9998 21.1974 23.9999 21V12.0005C23.9999 2.11469 16.0799 2.00049 15.9999 2.00049C15.7348 2.00049 15.4805 2.10578 15.293 2.2932C15.1054 2.48063 15 2.73486 14.9999 2.99999L14.9994 4.99999H13.9999C13.7915 4.99981 13.5883 5.06504 13.4189 5.18649L6.4189 10.1865C6.24484 10.3108 6.11521 10.4876 6.04905 10.691C5.98289 10.8943 5.98368 11.1136 6.0513 11.3165L7.0513 14.3165C7.12502 14.5388 7.27464 14.7281 7.47393 14.8512C7.67322 14.9743 7.90947 15.0234 8.1413 14.9898L13.0113 14.294L9.1518 20.47C9.04978 20.6332 8.99703 20.8224 8.99988 21.0149C9.00274 21.2073 9.06108 21.3949 9.1679 21.555L10.1314 23H9.9999C9.20452 23.0009 8.44196 23.3172 7.87954 23.8796C7.31712 24.4421 7.00077 25.2046 6.9999 26V30H25.9999V26C25.999 25.2046 25.6827 24.4421 25.1203 23.8796C24.5578 23.3172 23.7953 23.0009 22.9999 23ZM15.848 13.53C15.9491 13.3684 16.0018 13.1812 16 12.9906C15.9983 12.8 15.9421 12.6138 15.838 12.4541C15.734 12.2943 15.5865 12.1677 15.4128 12.089C15.2392 12.0103 15.0467 11.9829 14.858 12.01L8.6842 12.8919L8.1817 11.3841L14.3199 7.00049H15.9989C16.264 7.00049 16.5183 6.8952 16.7058 6.70777C16.8933 6.52034 16.9988 6.26612 16.9989 6.00099L16.9999 4.12889C18.5012 4.46359 21.2166 5.66999 21.8599 9.99999H18.9999V12H21.9999V14H18.9999V16H21.9999V18H18.9999V20H21.9999V20.6973L20.4647 23H12.5347L11.1899 20.9824L15.848 13.53ZM23.9999 28H8.9999V26C9.00014 25.7348 9.10557 25.4806 9.29305 25.2931C9.48054 25.1057 9.73475 25.0002 9.9999 25H22.9999C23.2651 25.0001 23.5193 25.1056 23.7068 25.2931C23.8943 25.4806 23.9997 25.7348 23.9999 26V28Z'
      fill='#662D91'
    />
  </svg>
)

const Product = () => {
  const data = {
    main: {
      title: 'Products',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet condimentum eu at diam consectetur mauris urna orci. Ac, dolor, nunc ac nunc, maecenas tortor ac. A, integer massa risus adipiscing dignissim arcu, sed enim viverra. Vestibulum orci aenean augue quis velit lobortis viverra praesent. Faucibus proin felis scelerisque amet purus ut lobortis. Sem dignissim aliquam in consectetur. Habitant urna ultrices lectus sit sodales porttitor adipiscing quam. '
    },
    sub: {
      title: 'Made for Business',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet condimentum eu at diam consectetur mauris urna orci. Ac, dolor, nunc ac nunc, maecenas tortor ac. A, integer massa risus adipiscing dignissim arcu, sed enim viverra. Vestibulum orci aenean augue quis velit lobortis viverra praesent. Faucibus proin felis scelerisque amet purus ut lobortis. '
    }
  }

  const pages = [Page1Image, Page2Image, Page3Image]

  const siteTemplates = [
    {
      Icon: KniveSvg,
      label: 'Cafe/ Restaurant Starter Kit'
    },
    {
      Icon: IndustrySvg,
      label: 'Business Starter Kit'
    },
    {
      Icon: KnightSvg,
      label: 'Recruitment Platform'
    }
  ]

  return (
    <div className='bg-white'>
      <div className='w-4/5 mx-auto pt-24 space-y-12'>
        <TitledDescription title={data.main.title} description={data.main.description} />
        <TitledDescription2 title={data.sub.title} description={data.sub.description} />

        <div className='lg:grid grid-cols-8 gap-6 items-center space-y-8 lg:space-y-0'>
          <div className='col-span-5 grid grid-cols-3 gap-4'>
            {pages.map((page, i) => (
              <div key={i} className={i===1 ? "pt-20 sm:pt-40": ""}>
                <Image src={page} alt={`page-${i + 1}-image`} />
              </div>
            ))}
          </div>
          <div className='col-span-3 flex flex-wrap justify-around'>
            {
              siteTemplates.map(({Icon, label}, i) => (
                <div key={i} className={`p-4 sm:p-12 lg:px-8 lg:py-6 bg-gray-200 rounded text-center w-2/5 flex flex-col justify-center items-center ${i===2 && "mt-8"}`}>
                  <div className="w-min p-3 rounded-full bg-gray-50">
                    <Icon />
                  </div>
                  <p className="mt-2 text-sm sm:text-base font-medium">{label}</p>
                </div>
              ))
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default Product
