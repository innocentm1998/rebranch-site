import CustomButton from "../ui/CustomButton";

const Subscription = () => {
  return (
    <div className='w-full bg-white pt-10'>
      <div className='w-3/4 mx-auto bg-prim1 text-center py-16 rounded-2xl'>
        <p className='text-2xl sm:text-4xl font-bold font-heading text-white px-4'>Get the latest updates ! Emailed to you</p>
        <div className="w-11/12 lg:w-2/3 mx-auto mt-8 md:bg-white rounded-md gap-y-2 flex flex-wrap justify-between items-center">
          <input placeholder="Your Email Address" className="w-full md:w-auto flex-grow p-5 text-lg md:bg-transparent focus:outline-none focus:bg-gray-200 rounded-md md:rounded-none md:rounded-tl-md md:rounded-bl-md min-w-0" />
          <div className="px-2 py-1 flex justify-center mx-auto">
            <CustomButton name="Get Started" fullWidth href="#" accent="prim2" />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Subscription;